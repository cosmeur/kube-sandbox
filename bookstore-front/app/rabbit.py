import pika
import os

def on_message(channel, method_frame, header_frame, body):
    print "message received: {}, {}, {}, {}".format(body, channel, method_frame, header_frame)
    os.environ["BOOKSTORE_FRONT_MSG"] = body
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)

def init_consume():
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq.test-rabbitmq.svc.cluster.local'))
    channel = connection.channel()
    channel.basic_consume(on_message, 'message')
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    connection.close()

def publish_message(message, queue):
    connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq.test-rabbitmq.svc.cluster.local'))
    channel = connection.channel()
    channel.basic_publish(exchange='',
                      routing_key=queue,
                      body=message)